<?php

namespace App\Providers;

use App\Repositories\Answer\AnswerRepository;
use App\Repositories\Answer\AnswerRepositoryInterface;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\Course\CourseRepository;
use App\Repositories\Course\CourseRepositoryInterface;
use App\Repositories\Learn\LearnRepository;
use App\Repositories\Learn\LearnRepositoryInterface;
use App\Repositories\Lesson\LessonRepository;
use App\Repositories\Lesson\LessonRepositoryInterface;
use App\Repositories\Question\QuestionRepository;
use App\Repositories\Question\QuestionRepositoryInterface;
use App\Repositories\Repository;
use App\Repositories\RepositoryInterface;
use App\Repositories\Result\ResultRepository;
use App\Repositories\Result\ResultRepositoryInterface;
use App\Repositories\ResultAnswer\ResultAnswerRepository;
use App\Repositories\ResultAnswer\ResultAnswerRepositoryInterface;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Role\RoleRepositoryInterface;
use App\Repositories\Test\TestRepository;
use App\Repositories\Test\TestRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\Video\VideoRepository;
use App\Repositories\Video\VideoRepositoryInterface;
use App\Services\Course\CourseService;
use App\Services\Course\CourseServiceInterface;
use App\Services\Learn\LearnService;
use App\Services\Learn\LearnServiceInterface;
use App\Services\Lessons\LessonService;
use App\Services\Lessons\LessonServiceInterface;
use App\Services\Question\QuestionService;
use App\Services\Question\QuestionServiceInterface;
use App\Services\Test\TestService;
use App\Services\Test\TestServiceInterface;
use App\Services\User\UserService;
use App\Services\User\UserServiceInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RepositoryInterface::class, Repository::class);
        #Category
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);
        #video
        $this->app->bind(VideoRepositoryInterface::class, VideoRepository::class);
        #Course
        $this->app->bind(CourseRepositoryInterface::class, CourseRepository::class);
        $this->app->bind(CourseServiceInterface::class, CourseService::class);

        #User
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(UserServiceInterface::class, UserService::class);

        #Lession
        $this->app->bind(LessonRepositoryInterface::class, LessonRepository::class);
        $this->app->bind(LessonServiceInterface::class, LessonService::class);

        #Test
        $this->app->bind(TestRepositoryInterface::class, TestRepository::class);
        $this->app->bind(TestServiceInterface::class, TestService::class);

        #Question
        $this->app->bind(QuestionRepositoryInterface::class, QuestionRepository::class);
        $this->app->bind(QuestionServiceInterface::class, QuestionService::class);

        #Learn
        $this->app->bind(LearnRepositoryInterface::class, LearnRepository::class);
        $this->app->bind(LearnServiceInterface::class, LearnService::class);

        #Answer
        $this->app->bind(AnswerRepositoryInterface::class, AnswerRepository::class);
        #Result
        $this->app->bind(ResultRepositoryInterface::class, ResultRepository::class);

        #Result Answer
        $this->app->bind(ResultAnswerRepositoryInterface::class, ResultAnswerRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
