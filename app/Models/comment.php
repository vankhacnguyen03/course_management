<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    use HasFactory;
    protected $fillable=[
        'user_id','course_id','reply_id','content'
    ];

    public function users(){
        return $this->hasone(User::class ,'id','user_id');
    }
    public function  child(){
        return $this->hasMany(comment::class,'reply_id','id');
    }
}
