<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lesson extends Model
{
    use HasFactory;
    use softDeletes;

    protected  $dates = ['deleted_at'];
    protected $fillable = [
        'name',
        'description',
        'content',
        'type',
        'course_id',
        'object_id',
        'status'
    ];

    public function course() {
        return $this->belongsTo(Course::class, 'course_id');
    }
}
