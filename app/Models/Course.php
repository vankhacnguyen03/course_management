<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use HasFactory;
    use softDeletes;
    protected  $dates = ['deleted_at'];
    protected $fillable = [
        'name','description','content','image','category_id','status','user_id'
    ];

    public function category() {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function learns() {
        return $this->hasMany(Learn::class,'course_id', 'id');
    }

    public function lessons() {
        return $this->hasMany(Lesson::class, 'course_id', 'id');
    }

}
