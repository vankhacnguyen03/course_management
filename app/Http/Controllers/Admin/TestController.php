<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Question\QuestionRepository;
use App\Services\Question\QuestionServiceInterface;
use App\Services\Test\TestService;
use App\Services\Test\TestServiceInterface;
use Illuminate\Http\Request;

class TestController extends Controller
{
    protected $testService;
    protected $questionService;
    protected $questionRepository;

    public function __construct(TestServiceInterface     $testService,
                                QuestionServiceInterface $questionService,
                                QuestionRepository       $questionRepository
    )
    {
        $this->testService = $testService;
        $this->questionService = $questionService;
        $this->questionRepository = $questionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tests = $this->testService->getTestAndQuestion();

        return view('admin.test.index', compact('tests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.test.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only('title', 'description');

        $this->testService->create($data);

        return redirect('admin/test');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $test = $this->testService->show($id);
        $questions = $this->questionService->getQuestionByTest($id);

        return view('admin.test.show', compact("test", "questions"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $test = $this->testService->show($id);

        return view('admin.test.edit', compact('test'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->only('title', 'description');

        $this->testService->update($data, $id);

        return redirect('admin/test');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->testService->delete($id);

        return redirect('admin/test');
    }
}
