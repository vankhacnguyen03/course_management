<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\LessonRequest\LessonCreateRequest;
use App\Models\Lesson;
use App\Repositories\Course\CourseRepository;
use App\Repositories\Lesson\LessonRepository;

use Illuminate\Support\Facades\Session;
use App\Services\Lessons\LessonService;
use Illuminate\Http\Request;

class LessonController extends Controller
{

    protected  $lessonService;
    protected  $lessonRepository;
    protected $courseRepository;

    public function __construct(LessonService $lessonService,
                                LessonRepository $lessonRepository,
                                CourseRepository $courseRepository)
    {
        $this->lessonService = $lessonService;
        $this->lessonRepository = $lessonRepository;
        $this->courseRepository = $courseRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $keyword = $request->key_word;
        $lessons = $this->lessonRepository->getQuestionByCondition($keyword);

        return view('admin.lesson.index', compact('lessons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = $this->courseRepository->all();

        return view('admin.lesson.create', compact("courses"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $data = $request->only('course_id','name','description','type','content','object_id');

        $result =$request->only('name','description','content','object_id','type','course_id','status');
        $result['status'] = $request->input('status') ? 1 : 0;
        if (!$result) {

            return redirect()->back()->with('no','thêm mới thất bại');
        }
        $this->lessonRepository->create($result);

        return redirect()->route('admin.lesson.index')->with('yes','thêm mới thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lesson = $this->lessonRepository->show_id($id);

        return view('admin.lesson.show' ,compact('lesson'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lessons = $this->lessonRepository->show($id);
        $courses = $this->courseRepository->all();
        return view('admin.lesson.edit', compact('lessons','courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $result = $this->lessonRepository->update($request->only('name','description','content','object_id','type','course_id','status'), $id);

        if ($result) {
            return redirect()->route('admin.lesson.index')->with('status', 'Update category successfully');
        }
        return redirect()->back()->with('status', 'update category fails');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->lessonRepository->delete($id);
        if ($result) {

            return redirect()->back()->with('yes','xóa thành công');
        }

        return redirect()->back()->with('yes','xóa thất bại');
    }
}
