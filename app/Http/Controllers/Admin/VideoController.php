<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\VideoFormRequest;
use App\Repositories\Video\VideoRepository;
use App\Services\Video\VideoService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;


class VideoController extends Controller
{

    protected $videoRepository;
    protected $videoService;

    public function __construct(VideoRepository $videoRepository, VideoService $videoService)
    {
        $this->videoRepository = $videoRepository;
        $this->videoService = $videoService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd($this->videoRepository->all());
        return view('admin.videos.list', [
            'videos' => $this->videoRepository->all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.videos.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoFormRequest $request)
    {
        $result = $this->videoRepository->create($request->only('name', 'video_path'));
        if (!$result) {
            Session::flash('error', 'Add videos fails');
            return redirect()->back();
        }

        Session::flash('success', 'Add videos successfully');
        return redirect()->route('list_videos');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = $this->videoRepository->show($id);
        //dd($video);

        if ($video) {
            $filePath = $video->video_path;
//            dd($filePath);
            if (File::exists($filePath)) {
                unlink($filePath);
            }
            $this->videoRepository->delete($id);
            Session::flash('success', 'Delete video successfully');
            return redirect()->back();
        }
        Session::flash('error', 'Delete video fails');
        return redirect()->back();
    }
}
