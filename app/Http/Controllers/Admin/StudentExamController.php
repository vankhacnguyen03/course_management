<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Answer\AnswerRepository;
use App\Repositories\Question\QuestionRepository;
use App\Repositories\Result\ResultRepository;
use App\Repositories\ResultAnswer\ResultAnswerRepository;
use App\Repositories\Test\TestRepository;
use App\Services\Test\TestService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentExamController extends Controller
{
    protected $testService;
    protected $questionRepository;
    protected $answerRepository;
    protected $testRepository;
    protected $resultRepository;
    protected $resultAnswerRepository;

    public function __construct(QuestionRepository     $questionRepository,
                                AnswerRepository       $answerRepository,
                                TestRepository         $testRepository,
                                TestService            $testService,
                                ResultRepository       $resultRepository,
                                ResultAnswerRepository $resultAnswerRepository
    )
    {
        $this->testRepository = $testRepository;
        $this->questionRepository = $questionRepository;
        $this->answerRepository = $answerRepository;
        $this->testService = $testService;
        $this->resultRepository = $resultRepository;
        $this->resultAnswerRepository = $resultAnswerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()) {
            $idUser = Auth::id();
        }
        $results = $this->resultRepository->getResultStudentById($idUser);
        //dd($result);
        return view('admin.studentexam.list', [
            'results' => $results
        ]);
    }

    public function showResultAnswer($idResult)
    {
        $result = $this->resultRepository->show($idResult);
        //dd($results);
        $resultsAnswers = $this->resultAnswerRepository->getResultAnswerByResult($result->id);
        return view('admin.studentexam.detail_exam', [
            'resultsAnswers' => $resultsAnswers,
            'result' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $scoreDefault = 100;
        $scoreCurrent = 0;
        $countQuestion = count($request->input('question_id'));
        for ($i = 0; $i < $countQuestion; $i++) {
            $idAnswer = $request->input('question_' . $i . '_option');
            if ($idAnswer) {
                $answer = $this->answerRepository->show($idAnswer);
                if ($answer->correct == 1) {
                    $scoreCurrent += $scoreDefault / $countQuestion;
                }
            } else {
                $scoreCurrent += 0;
            }

        }
        $newResult = [
            'test_name' => $request->input('test_name'),
            'score' => $scoreCurrent,
            'user_id' => Auth::id(),
        ];

        // dd($newResult);
        $result = $this->resultRepository->create($newResult);
        foreach ($request->input('question_id') as $key => $question_id) {
            $answerId = $request->input('question_' . $key . '_option');
            $newResultAnswer = [
                'question' => $this->questionRepository->show($question_id)->question,
                'answer' => $this->answerRepository->show($answerId)->answers,
                'result_id' => $result->id
            ];

            $this->resultAnswerRepository->create($newResultAnswer);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $test = $this->testService->show($id);
        $questions = $this->testService->getQuestionByTest($id);
        foreach ($questions as $question) {
            $options[] = $this->answerRepository->getAnswers($question->id);
        }
        return view('admin.studentexam.index', compact('test', 'questions', 'options', 'test'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
