<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\comment;
use App\Repositories\Comment\CommentRepository;
use App\Repositories\Course\CourseRepository;
use App\Repositories\Lesson\LessonRepository;
use App\Repositories\User\UserRepository;
use App\Services\Course\CourseServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AjaxCommentController extends Controller
{
    protected $courseRepository;
    protected $userRepository;
    protected $commentRepository;

//    protected $LessonRepository;


    public function __construct(CourseRepository $courseRepository,
                                UserRepository  $userRepository,
                                CommentRepository    $commentRepository
//                                LessonRepository $lessonRepository
    )
    {
        $this->courseRepository = $courseRepository;
        $this->userRepository = $userRepository;
        $this->commentRepository = $commentRepository;

//        $this->LessonRepository = $lessonRepository;
    }

    public function index(Request $request)
    {

        $courses = $this->courseRepository->all();
        $comments = $this->commentRepository->all();
//        dd($comments);
        return view('admin.test_comment.test',compact('courses','comments'));
    }

    public function comment($course_id,Request $req)
    {

        $users = Auth::user()->id;
        $data = [
            'course_id' => $course_id,
            'user_id' => $users,
            'content' => $req->content
        ];
        if($data){
           $this->commentRepository->create($data);
           $comments = comment::where(['course_id' => $course_id ,'reply_id' => 0])->get();
//           dd($comments);
           return view('admin.test_comment.list-comment',compact('comments'));
        }
      return redirect()->back()->with('no','that bai');

    }
}
