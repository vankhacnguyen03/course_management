<?php

namespace App\Services\Category;

use App\Repositories\Category\CategoryRepositoryInterface;

class CategoryService
{
    public $categoryRepository;


    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
    )
    {
        $this->categoryRepository = $categoryRepository;
    }
}
