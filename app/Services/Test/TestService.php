<?php

namespace App\Services\Test;

use App\Repositories\Question\QuestionRepository;
use App\Repositories\Test\TestRepositoryInterface;
use App\Services\Service;

class TestService extends  Service implements TestServiceInterface
{
    public  $repository;
    protected $questionRepository;

    public function __construct(TestRepositoryInterface $courseRepository,
    QuestionRepository $questionRepository
    )
    {
        $this->questionRepository = $questionRepository;
        $this->repository= $courseRepository;
    }
    public function getTestAndQuestion() {
        return $this->repository->getTestAndQuestion();
    }
    public function getQuestionByTest($id){
        return $this->questionRepository->getQuestionByTest($id);
    }
}
