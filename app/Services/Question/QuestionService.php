<?php

namespace App\Services\Question;

use App\Repositories\Answer\AnswerRepository;
use App\Repositories\Question\QuestionRepository;
use App\Repositories\Test\TestRepository;
use App\Services\Service;

class QuestionService extends Service implements QuestionServiceInterface
{
    protected $questionRepository;
    protected $answerRepository;
    protected $testRepository;

    public function __construct(QuestionRepository $questionRepository,
                                AnswerRepository $answerRepository,
                                TestRepository $testRepository
    )


    {
        $this->testRepository = $testRepository;
        $this->questionRepository= $questionRepository;
        $this->answerRepository = $answerRepository;
    }

    public function showOptionQuestion($id)
    {
     return   $this->answerRepository->getAnswers($id);
    }


    public function getNameTest($id){
        return $this->questionRepository->getNameTest($id);
    }

    public function insert($request){
        $data = $request;
        return $this->repository->create($data);
    }

    public function update($request, $id)
    {

        $result = $this->repository->update($request,$id);
        if ($result) {
            return true;
        }
        return false;
    }
    public function destroy($id)
    {
        $result = $this->repository->show($id);
        if ($result) {
            $this->repository->delete($id);
            return true;
        }
        return false;
    }

    public function getQuestionByTest($id) {
        return $this->questionRepository->getQuestionByTest($id);
    }
}
