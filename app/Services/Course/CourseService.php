<?php

namespace App\Services\Course;

use App\Repositories\Course\CourseRepositoryInterface;
use App\Services\Service;

class CourseService extends  Service implements CourseServiceInterface
{
    public  $repository;

    public function __construct(CourseRepositoryInterface $courseRepository)
    {
        $this->repository= $courseRepository;
    }

    public function getCourseAndLessons($searchBy, $keyword) {
        return $this->repository->getCourseAndLessons($searchBy, $keyword);
    }

    public function getCoursePaginate($paginate) {
        return $this->repository->getCoursePaginate($paginate);
    }

}
