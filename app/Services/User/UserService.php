<?php

namespace App\Services\User;

use App\Repositories\User\UserRepositoryInterface;
use App\Services\Service;

class UserService extends  Service implements UserServiceInterface
{
    public  $repository;

    public function __construct(UserRepositoryInterface $categoryRepository)
    {
        $this->repository= $categoryRepository;
    }
}
