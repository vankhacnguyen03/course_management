<?php

namespace App\Services\Video;


use App\Repositories\Video\VideoRepositoryInterface;

class VideoService
{
    public $repository;

    public function __construct(VideoRepositoryInterface $videoRepository)
    {
        $this->repository = $videoRepository;
    }

    public function insert($request)
    {
        if ($request->hasFile('path')) {
            $file = $request->file('path');
            $ext = $file->getClientOriginalName();
            $fileName = time() . '.' . $ext;
            $file->move('uploads', $fileName);
        }
        $data = ['name' => $request->input('name'),
            'video_path' => $fileName];
        return $this->repository->create($data);
    }
}
