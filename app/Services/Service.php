<?php

namespace App\Services;

class Service implements ServiceInterface
{
    public $repository;


    public function all()
    {
        return $this->repository->all();
    }

    public function show($id)
    {
       return $this->repository->show($id);
    }

    public function create(array $data)
    {
        return $this->repository->create($data);
    }

    public function update(array $data, $id)
    {
        return $this->repository->update($data,$id);
    }

    public function delete($id)
    {
        return $this->repository->delete($id);
    }

    public function searchAndPaginate($searchBy, $keyword) {
        return $this->repository->searchAndPaginate($searchBy, $keyword);
    }
}
