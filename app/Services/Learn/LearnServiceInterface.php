<?php

namespace App\Services\Learn;

use App\Services\ServiceInterface;

interface LearnServiceInterface extends ServiceInterface
{
    public function getStudentCount($id);

    public function getUserName($id);

    public function getCourseByLearn($id);

    public function getStudentCourse($id);

    public function getStudentCourses($id);
}
