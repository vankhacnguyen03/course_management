<?php

namespace App\Services\Learn;

use App\Repositories\Learn\LearnRepositoryInterface;
use App\Services\Service;

class LearnService extends  Service implements LearnServiceInterface
{
    public  $repository;

    public function __construct(LearnRepositoryInterface $courseRepository)
    {
        $this->repository= $courseRepository;
    }

    public function getStudentCount($id) {
        return $this->repository->getStudentCount($id);
    }

    public function getUserName($id) {
        return $this->repository->getUserName($id);
    }

    public function getCourseByLearn($id) {
        return $this->repository->getCourseByLearn($id);
    }

    public function getStudentCourse($id) {
        return $this->repository->getStudentCourse($id);
    }

    public function getStudentCourses($id) {
        return $this->repository->getStudentCourses($id);
    }
}
