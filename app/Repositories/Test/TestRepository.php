<?php

namespace App\Repositories\Test;

use App\Models\Test;
use App\Repositories\Repository;



class TestRepository extends Repository implements TestRepositoryInterface
{

    public function getModel()
    {
        return Test::class;
    }

    public function getTestAndQuestion() {
        return $this->model->with("questions")->get();
    }

    public function getQuestionByTest($id) {
        return $this->model->with('questions')->where('id', $id)->get();
    }

}
