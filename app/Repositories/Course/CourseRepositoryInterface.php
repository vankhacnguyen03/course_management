<?php

namespace App\Repositories\Course;

use App\Repositories\RepositoryInterface;

interface CourseRepositoryInterface extends RepositoryInterface
{
    public function getCourseByCondition();

    public function getCourseAndLessons($searchBy, $keyword);

    public function getCoursePaginate($paginate);
}
