<?php

namespace App\Repositories\ResultAnswer;

use App\Repositories\RepositoryInterface;

interface ResultAnswerRepositoryInterface extends RepositoryInterface
{
    public function getResultAnswerByResult($id);
}
