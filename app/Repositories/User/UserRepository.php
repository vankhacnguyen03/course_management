<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\Repository;
use Illuminate\Support\Facades\DB;


class UserRepository extends Repository implements UserRepositoryInterface
{

    public function getModel()
    {
        return User::class;
    }
}
