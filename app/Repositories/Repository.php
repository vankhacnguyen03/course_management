<?php

namespace App\Repositories;

abstract class Repository implements RepositoryInterface
{
    protected $model;

    public function __construct()
    {
        $this->model = app()->make($this->getModel());
    }

    abstract public function getModel();

    public function all()
    {

        return $this->model->all();
    }

    public function show(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function create(array $data)
    {

        return $this->model->create($data);
    }

    public function update(array $data, $id)
    {
        $obj = $this->model->find($id);
        return $obj->update($data);
    }


    public function delete($id)
    {
        $obj = $this->model->find($id);
        return $obj->delete();
    }

    public function searchAndPaginate($searchBy, $keyword)
    {
        return $this->model
            ->where($searchBy, 'like', '%' . $keyword . '%')
            ->orderBy('id', 'desc')
            ->paginate(6)
            ->appends(['search' => $keyword]);
    }

    public function show_id($id)
    {
        return $this->model->findOrFail($id)->with('course')->get();
    }


}
