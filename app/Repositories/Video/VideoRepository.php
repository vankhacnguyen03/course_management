<?php

namespace App\Repositories\Video;

use App\Models\Video;
use App\Repositories\Repository;



class VideoRepository extends Repository implements VideoRepositoryInterface
{
    public function getModel()
    {
        return Video::class;
    }
}
