<?php

namespace App\Repositories\Comment;


use App\Models\comment;
use App\Repositories\Comment\CommentRepositoryInterface;
use App\Repositories\Repository;



class CommentRepository extends Repository implements CommentRepositoryInterface
{
    public function getModel()
    {
        return comment::class;
    }

    public function getAnswers($course_id)
    {
        return $this->model->with('users')->where('user_id',$course_id)->get();
    }
}
