<?php

namespace App\Repositories\Answer;

use App\Repositories\RepositoryInterface;

interface AnswerRepositoryInterface extends RepositoryInterface
{
    public function getAnswers($idQuestion);

    public function getOptionByQuestion($id);
}

