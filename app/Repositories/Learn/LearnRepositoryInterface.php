<?php

namespace App\Repositories\Learn;

use App\Repositories\RepositoryInterface;

interface LearnRepositoryInterface extends RepositoryInterface
{
    public function getStudentCount($id);

    public function getUserName($id);

    public function getCourseByLearn($id);

    public function getStudentCourse($id);

    public function getStudentCourses($id);
}
