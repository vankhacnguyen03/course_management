<?php

namespace App\Repositories\Permission;

use App\Repositories\Repository;
use Spatie\Permission\Models\Permission;

class PermissionRepository extends Repository implements PermissionRepositoryInterface
{

    public function getModel()
    {
        return Permission::class;
    }

    public function getDataPermission($column, $data)
    {
        $this->model->where($column,$data)->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')->get();
    }
    public function getNameRoleHasPermissions($id){
        return $this->model
            ->with("permissions")
            ->join('role_has_permissions',"role_has_permissions.permission_id", "=", "permissions.id")
            ->where("role_has_permissions.role_id", $id)
            ->get();
    }
}
