<?php

namespace App\Repositories\Permission;

use App\Repositories\RepositoryInterface;

interface PermissionRepositoryInterface extends RepositoryInterface
{
    public function getDataPermission($column, $data);

    public function getNameRoleHasPermissions($id);
}
