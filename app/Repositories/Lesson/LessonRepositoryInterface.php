<?php

namespace App\Repositories\Lesson;

use App\Repositories\RepositoryInterface;

interface LessonRepositoryInterface extends RepositoryInterface
{
    public function getLessonsByCourse($id);

    public function getCourseByLearn($id);

    public function getCourseID($id);

    public function getFirstLessonByCourse($id);
}

