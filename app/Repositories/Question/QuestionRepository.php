<?php

namespace App\Repositories\Question;


use App\Models\Question;
use App\Repositories\Repository;

class QuestionRepository extends Repository implements QuestionRepositoryInterface
{
    public function getModel()
    {
        return Question::class;
    }
    public function getQuestionByCondition($keyword = null)
    {
        return $this->model->when($keyword != null, function ($query) use ($keyword) {
            $query->where('question', 'like', '%' . $keyword . '%');
        })->with('test')->get();
    }

    public function getQuestionByTest($id) {
        return $this->model->with('test')->where('test_id', $id)->get();
    }

    public function getOptionByQuestion($id) {
        return $this->model->with('answers')->where('id', $id)->get();
    }

//    public function getNameTest($id){
//        return $this->model->with('test')->where('id',$id)->get();
//    }
}
