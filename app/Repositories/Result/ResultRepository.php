<?php

namespace App\Repositories\Result;

use App\Models\Result;
use App\Repositories\Repository;

class ResultRepository extends Repository implements ResultRepositoryInterface
{

    public function getModel()
    {
        return Result::class;
    }

    public function getResultStudentById($idUser)
    {
        return $this->model->where('user_id',$idUser)->get();
    }
}
