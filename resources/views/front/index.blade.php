@extends('front.layout.master')
@section('title', 'Home')
@section('content')
    <div class="col-md-11 main__right">
        <!-- Content here -->
        <div class="container-fluid">
            <div class="slider row">
                <div class="col-md-6 slider__content">
                    <div class="slider__title">
                        Khóa học HTML CSS
                    </div>
                    <div class="slider__desc">
                        Đây là khóa học đầy đủ và chi tiết nhất bạn có thể tìm thấy được ở trên Internet!
                    </div>

                    <a class="btn slider__btn">
                        Tìm hiểu thêm
                    </a>
                </div>
                <img src="../front/assets/img/slider.jpg" alt="" class="slider__img">
            </div>
        </div>

        <div class="box-learn container">
            <div class="row box-top">
                <div class="box__title">
                    Khóa Học nổi bật
                </div>
            </div>
            <div class="row box-bottom">
                @foreach($coursesPaginate as $course)
                    <div class="col-md-3 box__item">
                        <div class="box__item-top">
                            <img src="../front/img/course_image/{{ $course->image }}" alt="" class="box__item-img">
                            <div class="overlay-black"></div>
                            <a href="./student/{{ $course->id }}" class="box__item-top--btn">
                                Xem khóa học
                            </a>
                        </div>
                        <h3 class="box__item-title">
                            {{ $course->name }}
                        </h3>
                        <div class="box__item-price">
                                    <span class="box__item-price--old">
                                        2.499.000đ
                                    </span>
                            <span class="box__item-price--new">
                                        1.299.000đ
                                    </span>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>

        <div class="box-learn container">
            <div class="row box-top">
                <div class="box__title">
                    Tất cả khóa học
                </div>
            </div>
            <div class="row box-bottom">
                @foreach($courses as $course)
                    <div class="col-md-3 box__item">
                        <div class="box__item-top">
                            <img src="../front/img/course_image/{{ $course->image }}" alt="" class="box__item-img">
                            <div class="overlay-black"></div>
                            <a href="./student/{{ $course->id }}" class="box__item-top--btn">
                                Xem khóa học
                            </a>
                        </div>
                        <h3 class="box__item-title">
                            {{ $course->name }}
                        </h3>
                        <div class="box__item-price">
                                    <span class="box__item-price--old">
                                        2.499.000đ
                                    </span>
                            <span class="box__item-price--new">
                                        1.299.000đ
                                    </span>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
        <!-- END -->
    </div>
@endsection
