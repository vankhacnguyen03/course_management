@extends('front.layout.master')
@section('title', 'Show')
@section('content')
    <div class="col-md-11 main__right">
        <!-- Content here -->
        <div class="container detail">
            <div class="row">
                <div class="col-md-8">
                    <div class="detail__head">
                        <div class="detail__head-name">
                            {{ $course->name }}
                        </div>

                        <div class="detail__head-desc">
                            {{ $course->description }}
                        </div>
                    </div>

                    <div class="detail__body">
                        <div class="detail__body-title">
                            Nội dung khóa học
                        </div>

                        <ul class="detail__body-list">
                            @if($studentCourse != null)
                                @foreach($lessons as $key => $lesson)
                                    <li class="detail__body-item">
                                        <a href="./{{ $lesson->id }}/learn" class="detail__body-link">
                                            {{ $key + 1 }}. {{ $lesson->name }}
                                        </a>
                                    </li>
                                @endforeach
                            @else
                                <li class="detail__body-item">
                                    <a href="" class="detail__body-link">
                                        Bạn chưa đăng ký khóa học
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>

                    <div class="detail__relate">
                        <div class="detail__relate-title">
                            Khóa học liên quan
                        </div>

                        <a href="/admin/student" class="detail__relate-item">
                            <img src="../../front/assets/img/relate.png" alt="" class="detail__relate-img">
                            <div class="detail__relate-right">
                                <div class="detail__relate-name">
                                    HTML CSS Pro
                                </div>
                                <div class="detail__relate-price">
                                    <p class="detail__relate-price--old">2.499.000đ</p>
                                    <p class="detail__relate-price--new">1.299.000đ</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="detail__right">
                        <img src="../../front/img/course_image/{{ $course->image }}" alt="" class="detail__right-img">
                            @if($studentCourse != null)
                                <a href="./{{ $first_lesson->id }}/learn" class="detail__right-btn">
                                    Vào học
                                </a>
                            @else
                                <a href="" class="detail__right-btn">
                                    Đăng ký học
                                </a>
                            @endif

                        <div class="detail__right-content">
                            <div class="detail__right-item">
                                <div class="detail__right-icon">
                                    <i class="fa-sharp fa-solid fa-graduation-cap"></i>
                                </div>
                                <div class="detail__right-desc">Trình độ cơ bản</div>
                            </div>

                            <div class="detail__right-item">
                                <div class="detail__right-icon">
                                    <i class="fa-sharp fa-solid fa-film"></i>
                                </div>
                                <div class="detail__right-desc">{{ count($lessons) }} bài học</div>
                            </div>

                            <div class="detail__right-item">
                                <div class="detail__right-icon">
                                    <i class="fa-solid fa-battery-three-quarters detail__right-icon"></i>
                                </div>
                                <div class="detail__right-desc">Trình độ cơ bản</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END -->
    </div>
@endsection
