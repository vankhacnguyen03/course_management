<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Learn</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
          integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" href="/front/assets/css/base.css">
    <link rel="stylesheet" href="/front/assets/css/style.css">
    <link rel="stylesheet" href="/front/assets/css/learn.css">
</head>
<body>
<div class="app">
    <div class="header container-fluid">
        <div class="header__left">
            <a href="/admin/student"><img src="/front/assets/img/logo.png" class="header__left-logo"></img></a>
            <div class="header__left-title">
                Học Lập Trình Để Đi Làm
            </div>
        </div>

        <div class="header__search">
            <i class="fa-solid fa-magnifying-glass header__search-icon"></i>
            <input type="text" class="header__search-input" placeholder="Tìm kiếm khóa học,...">
        </div>

        <div class="header__right">
            <div class="header__right-mylearn">
                Khóa học của tôi
                <div class="header__right-dropdown">

                    @if($studentCourses != null)
                        <h3 class="header__right-title">
                            Khóa học của tôi
                        </h3>
                        <ul class="header__right-list">
                            @foreach($studentCourses as $studentCourse)
                                <li class="header__right-item">
                                    <a href="#" class="header__right-link">
                                        <img src="/front/img/course_image/{{ $studentCourse->course->image }}"
                                             alt="" class="header__right-img">
                                        <div class="header__right-course">
                                            {{ $studentCourse->course->name }}
                                        </div>
                                    </a>
                                </li>
                            @endforeach

                            @else
                                <h3 class="header__right-title">
                                    Bạn chưa có khóa học nào
                                </h3>
                            @endif
                        </ul>
                </div>
            </div>

            <i class="fa-solid fa-bell header__right-icon"></i>
            <div class="header__right-user">
                <a href="{{ route('logout') }}"
                   class="btn-pill btn-shadow btn-shine btn btn-focus header__right-user-name"
                   onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();"
                >
                    @if(Auth::user() != null)
                        {{ Auth::user()->name }}
                    @else
                        <div>
                            <a href="{{ route('login') }}" class="header__right-login btn">
                                Đăng nhập
                            </a>
                        </div>
                    @endif
                </a>

                <form id="logout-form" action="{{ route('logout') }}"
                      method="POST" class="d-none">
                    @csrf
                </form>
            </div>


        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">
                <div class="left__col">
                    <video class="video" controls>
                        <source src="/front/assets/videos/i'mnotok.mp4" type="video/mp4">
                        Your browser does not support HTML video.
                    </video>
                </div>
                <div class="offset-md-1 col-md-12">
                    @foreach($course as $key => $item)
                        <div class="left__lesson-name">
                            {{ $item->course->name }}
                        </div>

                        <div class="left__lesson-description">
                            {{ $item->course->description }}
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-3 right__col">
                <div class="right__lesson-title">
                    Nội dung khóa học
                </div>
                @foreach($lessons as $key => $lesson)
                    <div class="right__lesson-item">
                        <a href="../{{ $lesson->id }}/learn" class="right__lesson-link">
                            {{ $key + 1 }}. {{ $lesson->name }}
                        </a>
                    </div>
                @endforeach
                <div class="right__lesson-item">
                    <a href="/admin/studentexam/{{ $test->id }}" class="right__lesson-link">
                        Examination
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer__learn">
        <button class="footer__left-btn">
            <a href="/admin/student/{{ $currentLearn - 1 }}/learn" class="footer__btn-link">Bài trước</a>
        </button>

        <button class="footer__right-btn">
            <a href="/admin/student/{{ $currentLearn + 1 }}/learn" class="footer__btn-link">Bài tiếp theo</a>
        </button>
    </div>
</div>
</body>
</html>
