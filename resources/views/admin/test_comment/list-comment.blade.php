@foreach($comments as $comment)

<div class="comment">
    <div class="media-body">
            <h4 class="media-heading"> {{$comment->users->name }}</h4>
            <p>{{$comment->content}}</p>

    </div>
</div>
@endforeach
