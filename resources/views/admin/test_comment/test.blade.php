@foreach($courses as $course)
@extends('admin.layouts_v2.master')
@section('content')
    <div class="content-wrapper">
        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Forms /</span> Basic Inputs</h4>
            <div class="row">
                <div class="col-md-6">
                    <div class="card mb-4">
                        <form method="post">
                            <h5 class="card-header">xin chào :{{ Auth::user()->name }}</h5>
                            <div class="card-body">
                                <div>
                                    <label for="defaultFormControlInput" class="form-label">content</label>
                                    <textarea required id="comment-content" class="form-control"></textarea>
                                    <small id="comment-error" class="help-blog"></small>
                                    <button class="btn btn-primary" id="btn-comment">submit</button>
                                </div>
                                <h3>các bình luận</h3>
                                <div id="comment">
                                    <div class="media-body">
                                        @foreach($comments as $comment)
                                        <h4 class="media-heading"> {{ Auth::user()->name }}</h4>
                                            <p>{{$comment->content}}</p>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
    let _csrf = '{{csrf_token()}}';
    $('#btn-comment').click(function (ev){
        ev.preventDefault();
        let content = $('#comment-content').val();
        let _commentUrl = '{{route("ajax.comment",$course->id)}}';
        // console.log(content,_commentUrl);
        $.ajax({
            url: _commentUrl,
            type: 'POST',
            data:{
                content:content,
                _token: _csrf,
            },
            success:function (res) {
                if(res.error){
                   $('#comment-error').html(res.error)
                }else {
                    $('#comment-error').html('');
                    $('#comment-content').val('');
                    $('#comment').html(res);
                   // console.log(res);
                }
            }
        })
    })

</script>
@endsection
@endforeach
