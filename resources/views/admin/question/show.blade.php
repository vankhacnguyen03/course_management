@extends('admin.layouts.master')
@section('body')
    <!-- Main -->
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body display_data">
                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label">Question: </label>
                            <div class="col-md-9 col-xl-8">
                                <p>{{ $question-> question }}</p>
                            </div>
                        </div>
                        @foreach($answers as $answer)

                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label">{{ $answer->answers }}</label>
                            <div class="col-md-9 col-xl-8">
                                @if($answer->correct == 1)
                                    <i class="fas fa-check"></i>
                                @else
                                @endif
                            </div>
                        </div>
                        @endforeach
                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label"></label>
                            <div class="col-md-9 col-xl-8">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main -->
@endsection
