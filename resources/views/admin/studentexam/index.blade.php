<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=\, initial-scale=1.0">
    <title>Student Exam</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css"
          integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="/dashboard/test_form.css">
    <link rel="stylesheet" href="/dashboard/base.css">
</head>
<body>
<div class="app">
    <div class="container">
        <div class="row form__header">
            <div class="form__header-time">
                <i class="fa-solid fa-stopwatch form__header-icon"></i>
                <span class="form__header-spc">59</span> M <span class="form__header-spc">59</span> S
            </div>
            <p class="form__header-desc">
                TIME LEFT IN THIS ASSIGNMENT SESSION
            </p>
        </div>

        <div class="row form__body">
            <div class="col-md-12 form__body-all-question">
            </div>
            <form class="col-md-12" method="post">
                @csrf
                {{--                {{ $k = 1 }}--}}
                <input hidden type="text" name="test_name" value="{{ $test->title }}">
                @foreach($questions as $key => $question)
                    <div class="row border-top" id="question">
                        <input hidden name="question_id[]" value="{{$question->id}}">
                        <div class="col-md-6 form__body-left">

                            <div class="form__body-bot">
                                <p class="form__body-desc">
                                    {{($key+1) . '.' .$question->question }}
                                </p>
                                <input hidden type="text" name="question[]" value="{{ $question->question }}">
                            </div>
                        </div>

                        <div class="col-md-6 form__body-right">
                            @foreach($options[$key] as $option)
                                <div class="form-check form__body-option">
                                    <input class="form__body-input" type="radio"
                                           name="question_{{ $key  }}_option"
                                           value="{{ $option->id }}"
                                           id="question_1_option-1">
                                    <label class="form__body-option-text" for="question_1_option-1">
                                        {{ $option->answers }}
                                    </label>

{{--                                    <input hidden type="text" name="question_answer_{{$option->id}}" value="{{ $option->answers }}">--}}
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
                <div class="row just-center mt-20">
                    <input type="submit" class="btn btn-primary btn-hover-shine finish_btn" value="Finish"></input>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
