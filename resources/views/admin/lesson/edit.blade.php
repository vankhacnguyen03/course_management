@extends('admin.layouts.master')

@section('body')

    <div class="app-main__inner">

        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-ticket icon-gradient bg-mean-fruit"></i>
                    </div>
                    <div>
                        Form Create New Category
                        <div class="page-title-subheading">
                            View, create, update, delete and manage.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <form action="" method="POST" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="position-relative row form-group">
                                <label for="user_id"
                                       class="col-md-3 text-md-right col-form-label">Course</label>
                                <div class="col-md-9 col-xl-8">
                                    <select required name="user_id" id="user_id"
                                            class="form-control">
                                        <option value="">-- Courses --</option>
                                        @foreach($courses as $course)
                                            <option value={{ $course->id }}>
                                                {{ $course->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">Name</label>
                                <div class="col-md-9 col-xl-8">
                                    <input required name="name" placeholder="Name" type="text"
                                           class="form-control" value="{{ $lessons -> name }}">
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="description"
                                       class="col-md-3 text-md-right col-form-label">Description</label>
                                <div class="col-md-9 col-xl-8">
                                    <textarea class="form-control" name="description"
                                              placeholder="Description">{{ $lessons->description }}</textarea>
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">type_lesson</label>
                                <div class="col-md-9 col-xl-8">
                                    <input required name="type_lesson" placeholder="Name" type="text"
                                           class="form-control" value="{{ $lessons -> type }}">
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">Object ID</label>
                                <div class="col-md-9 col-xl-8">
                                    <input required name="object_id" placeholder="Name" type="text"
                                           class="form-control" value="{{ $lessons -> object_id }}">
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="featured"
                                       class="col-md-3 text-md-right col-form-label">Status</label>
                                <div class="col-md-9 col-xl-8">
                                    <div class="position-relative form-check pt-sm-2">
                                        <input name="status" id="featured" type="checkbox" value="1" {{ $lessons->status == 1 ? 'checked':'' }}
                                        class="form-check-input">
                                        <label for="featured" class="form-check-label"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="position-relative row form-group mb-1">
                                <div class="col-md-9 col-xl-8 offset-md-2">
                                    <a href="./admin/course" class="border-0 btn btn-outline-danger mr-1">
                                                    <span class="btn-icon-wrapper pr-1 opacity-8">
                                                        <i class="fa fa-times fa-w-20"></i>
                                                    </span>
                                        <span>Cancel</span>
                                    </a>

                                    <button type="submit"
                                            class="btn-shadow btn-hover-shine btn btn-primary">
                                                    <span class="btn-icon-wrapper pr-2 opacity-8">
                                                        <i class="fa fa-download fa-w-20"></i>
                                                    </span>
                                        <span>Save</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

@endsection
