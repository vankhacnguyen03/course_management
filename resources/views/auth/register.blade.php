<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="./dashboard/style.css">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
</head>
<body>
<div class="app">
    <div class="container">
        <div class="row flex-center">
            <div class="col-md-7 col-lg-5 col-sm-8 login__box">
                <h1 class="login__heading">
                    {{ __('Sign up') }}
                </h1>

                <form class="login__form" method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="login__form-group">
                        <label for="name" class="login__label">{{ __('Name') }}</label>
                        <input type="text" class=" login__input @error('name') is-invalid @enderror" id="name"
                               name="name" required autocomplete="name">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <div class="login__form-group">
                        <label for="email" class="login__label">{{ __('Email Address') }}</label>
                        <input type="email" class=" login__input @error('email') is-invalid @enderror" id="email" name="email" required autocomplete="email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <div class="login__form-group">
                        <label for="password" class="login__label">{{ __('Password') }}</label>
                        <input type="password" class="login__input @error('password') is-invalid @enderror" id="password" name="password" required autocomplete="new-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <div class="login__form-group">
                        <label for="password-confirm" class="login__label">{{ __('Confirm Password') }}</label>
                        <input type="password" class="login__input @error('password') is-invalid @enderror" id="password" name="password_confirmation" required autocomplete="new-password">
                    </div>


                    <button type="submit" class="login__btn">Sign in</button>
                </form>

                <p class="login__register">Have an account? <a href="./login">Sign in</a></p>
            </div>
        </div>
    </div>
</div>


</body>
</html>

