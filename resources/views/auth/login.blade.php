<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="./dashboard/style.css">
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
</head>
<body>
    <div class="app">
        <div class="container">
            <div class="row flex-center">
                <div class="col-md-7 col-lg-5 col-sm-8 login__box">
                    <h1 class="login__heading">
                        {{ __('Sign in') }}
                    </h1>

                    <form class="login__form" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="login__form-group">
                          <label for="email" class="login__label">{{ __('Email Address') }}</label>
                          <input type="email" class=" login__input @error('email') is-invalid @enderror" id="email" aria-describedby="emailHelp" name="email" required autocomplete="email" autofocus>
                          @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="login__form-group">
                          <label for="password" class="login__label">{{ __('Password') }}</label>
                          <input type="password" class="login__input @error('password') is-invalid @enderror" id="password" name="password" required autocomplete="current-password">
                          @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <button type="submit" class="login__btn">Sign in</button>
                    </form>

                    <p class="login__register">Not a member? <a href="./register">Sign up now</a></p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
