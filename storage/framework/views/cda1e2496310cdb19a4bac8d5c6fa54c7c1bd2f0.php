<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $__env->yieldContent('title'); ?> - Student Page</title>
    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css"
          integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" href="../../front/assets/css/base.css">
    <link rel="stylesheet" href="../../front/assets/css/style.css">
    <link rel="stylesheet" href="../../front/assets/css/course-detail.css">
</head>
<body>
<div class="app">
    <div class="header container-fluid">
        <div class="header__left">
            <a href="/admin/student"><img src="../../front/assets/img/logo.png" class="header__left-logo"></img></a>
            <div class="header__left-title">
                Học Lập Trình Để Đi Làm
            </div>
        </div>

        <div class="header__search">
            <i class="fa-solid fa-magnifying-glass header__search-icon"></i>
            <input type="text" class="header__search-input" placeholder="Tìm kiếm khóa học,...">
        </div>

        <div class="header__right">
            <div class="header__right-mylearn">
                Khóa học của tôi
                <div class="header__right-dropdown">


                    <h3 class="header__right-title">
                        Khóa học của tôi
                    </h3>
                    <ul class="header__right-list">
                        <?php $__currentLoopData = $studentCourses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $studentCourse): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="header__right-item">
                                <a href="/admin/student/<?php echo e($studentCourse->course->id); ?>" class="header__right-link">
                                    <img src="../../front/img/course_image/<?php echo e($studentCourse->course->image); ?>"
                                         alt="" class="header__right-img">
                                    <div class="header__right-course">
                                        <?php echo e($studentCourse->course->name); ?>

                                    </div>
                                </a>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>

            <i class="fa-solid fa-bell header__right-icon"></i>
            <div class="header__right-user">
                <a href="<?php echo e(route('logout')); ?>"
                   class="btn-pill btn-shadow btn-shine btn btn-focus header__right-user-name"
                   onclick="event.preventDefault();
                                                                document.getElementById('logout-form').submit();"
                >
                    <?php if(Auth::user() != null): ?>
                        <?php echo e(Auth::user()->name); ?>

                    <?php else: ?>
                        <div style="display: flex">
                            <a href="<?php echo e(route('login')); ?>" class="header__right-login btn">
                                Đăng nhập
                            </a>
                        </div>
                    <?php endif; ?>
                </a>

                <form id="logout-form" action="<?php echo e(route('logout')); ?>"
                      method="POST" class="d-none">
                    <?php echo csrf_field(); ?>
                </form>
            </div>


        </div>
    </div>

    <div class="main container-fluid">
        <div class="row">
            <div class="col-md-1 main__left">
                <div class="main__left-add">
                    <i class="fa-solid fa-plus plus-sm"></i>
                </div>
                <ul class="main__left-list">
                    <li class="main__left-item active">
                        <a href="/admin/student" class="main__left-link">
                            <i class="fa-sharp fa-solid fa-house"></i>
                            <p>Home</p>
                        </a>
                    </li>
                    <li class="main__left-item">
                        <a href="" class="main__left-link">
                            <i class="fa-solid fa-lightbulb"></i>
                            <p>Học</p>
                        </a>
                    </li>
                    <li class="main__left-item" >
                        <a href="" class="main__left-link"  >
                            <i class="fa-solid fa-newspaper"></i>
                            <p>Blog</p>
                        </a>
                    </li>
                </ul>
            </div>

            <?php echo $__env->yieldContent('content'); ?>

        </div>
    </div>

    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 footer__item">
                    <div class="footer__left footer__title">
                        <a href="./index.html"><img src="../../front/assets/img/logo.png" class="header__left-logo"></a>
                        <div class="header__left-title footer__item-title">
                            Học Lập Trình Để Đi Làm
                        </div>
                    </div>
                    <ul class="footer__list">
                        <li class="footer__list-item">
                            Điện thoại: <span>0246.329.1102</span>
                        </li>
                        <li class="footer__list-item">
                            Email: <span>contact@fullstack.edu.vn</span>
                        </li>
                        <li class="footer__list-item">
                            Địa chỉ: <span>Nhà D9, lô A10, Nam Trung Yên, Trung Hòa, Cầu Giấy, Hà Nội</span>
                        </li>
                    </ul>
                </div>

                <div class="col-sm-3 footer__item pl-100">
                    <div class="footer__title">
                        <div class="footer__item-title">
                            Về F8
                        </div>
                    </div>
                    <ul class="footer__list">
                        <li class="footer__list-item">
                            Giới thiệu
                        </li>
                        <li class="footer__list-item">
                            Cơ hội việc làm
                        </li>
                    </ul>
                </div>

                <div class="col-sm-3 footer__item pl-100">
                    <div class="footer__title">
                        <div class="footer__item-title">
                            Hỗ trợ
                        </div>
                    </div>
                    <ul class="footer__list">
                        <li class="footer__list-item">
                            Liên hệ
                        </li>
                        <li class="footer__list-item">
                            Bảo mật
                        </li>
                        <li class="footer__list-item">
                            Điều khoản
                        </li>
                    </ul>
                </div>

                <div class="col-sm-3 footer__item">
                    <div class="footer__title">
                        <div class="footer__item-title">
                            CÔNG TY CỔ PHẦN CÔNG NGHỆ GIÁO DỤC F8
                        </div>
                    </div>
                    <ul class="footer__list">
                        <li class="footer__list-item">
                            Mã số thuế: <span>0109922901</span>
                        </li>
                        <li class="footer__list-item">
                            Ngày thành lập: <span>04/03/2022</span>
                        </li>
                        <li class="footer__list-item">
                            Lĩnh vực: <span>Công nghệ, giáo dục, lập trình. F8 xây dựng và phát triển những sản phẩm mang lại giá trị cho cộng đồng.</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<?php /**PATH E:\xampp\htdocs\unica\resources\views/front/layout/master.blade.php ENDPATH**/ ?>