
<?php $__env->startSection('body'); ?>
    <!-- Main -->
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body display_data">
                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label">Name</label>
                            <div class="col-md-9 col-xl-8">
                                <p><?php echo e($lesson->name); ?></p>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label">Description</label>
                            <div class="col-md-9 col-xl-8">
                                <p><?php echo e($lesson->description); ?></p>
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label"><?php echo e($lesson->type); ?></label>
                            <div class="col-md-9 col-xl-8">
                                <p>type</p>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label"><?php echo e($lesson->content); ?></label>
                            <div class="col-md-9 col-xl-8">
                                <p>content</p>
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label"><?php echo e($lesson->object_id); ?></label>
                            <div class="col-md-9 col-xl-8">
                                <p>object_id</p>
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label"><?php echo e($lesson->course->name); ?></label>
                            <div class="col-md-9 col-xl-8">
                                <p>course_name</p>
                            </div>
                        </div>
                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label">Status</label>
                            <div class="col-md-9 col-xl-8">
                                <?php if($lesson->status == 1): ?>
                                    <p>Show</p>
                                <?php else: ?>
                                    <p>Hide</p>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\unica\resources\views/admin/lesson/show.blade.php ENDPATH**/ ?>