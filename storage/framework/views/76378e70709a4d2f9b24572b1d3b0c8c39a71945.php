<?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<div class="comment">
    <div class="media-body">
            <h4 class="media-heading"> <?php echo e($comment->users->name); ?></h4>
            <p><?php echo e($comment->content); ?></p>

    </div>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php /**PATH E:\xampp\htdocs\unica\resources\views/admin/test_comment/list-comment.blade.php ENDPATH**/ ?>