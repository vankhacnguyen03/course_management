<?php $__env->startSection('body'); ?>
    <!-- Main -->
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <form method="post" action="" enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <div class="position-relative row form-group">
                                <label for="description"
                                       class="col-md-3 text-md-right col-form-label">test_id</label>
                                <div class="col-md-9 col-xl-8">
                                    <select required name="test_id" id="user_id"
                                            class="form-control">
                                        <option value="">-- test_id --</option>
                                        <?php $__currentLoopData = $tests; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $test): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value=<?php echo e($test->id); ?>>
                                                <?php echo e($test->title); ?>

                                            </option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">Question </label>
                                <div class="col-md-9 col-xl-8">
                                    <textarea required name="question" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">option 1</label>
                                <div class="col-md-9 col-xl-8">
                                    <input required name="answer[]" id="description" placeholder="option 1" type="text"
                                           class="form-control" value="">
                                    <input type="radio" value="0" name="correct">
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">option 2</label>
                                <div class="col-md-9 col-xl-8">
                                    <input required name="answer[]" id="description" placeholder="option 2" type="text"
                                           class="form-control" value="">
                                    <input type="radio" name="correct" value="1">
                                </div>
                            </div>
                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">option 3</label>
                                <div class="col-md-9 col-xl-8">
                                    <input required name="answer[]" id="description" placeholder="option 3" type="text"
                                           class="form-control" value="">
                                    <input type="radio" name="correct" value="2">
                                </div>
                            </div>

                            <div class="position-relative row form-group">
                                <label for="name" class="col-md-3 text-md-right col-form-label">option 4</label>
                                <div class="col-md-9 col-xl-8">
                                    <input required name="answer[]" id="description" placeholder="option 4" type="text"
                                           class="form-control" value="">
                                    <input type="radio" name="correct" value="3">
                                </div>
                            </div>


                            <div class="position-relative row form-group mb-1">
                                <div class="col-md-9 col-xl-8 offset-md-2">
                                    <a href="#" class="border-0 btn btn-outline-danger mr-1">
                                                    <span class="btn-icon-wrapper pr-1 opacity-8">
                                                        <i class="fa fa-times fa-w-20"></i>
                                                    </span>
                                        <span>Cancel</span>
                                    </a>

                                    <button type="submit"
                                            class="btn-shadow btn-hover-shine btn btn-primary">
                                                    <span class="btn-icon-wrapper pr-2 opacity-8">
                                                        <i class="fa fa-download fa-w-20"></i>
                                                    </span>
                                        <span>Save</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\unica\resources\views/admin/question/create.blade.php ENDPATH**/ ?>