<?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<?php $__env->startSection('content'); ?>
    <div class="content-wrapper">
        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Forms /</span> Basic Inputs</h4>
            <div class="row">
                <div class="col-md-6">
                    <div class="card mb-4">
                        <form method="post">
                            <h5 class="card-header">xin chào :<?php echo e(Auth::user()->name); ?></h5>
                            <div class="card-body">
                                <div>
                                    <label for="defaultFormControlInput" class="form-label">content</label>
                                    <textarea required id="comment-content" class="form-control"></textarea>
                                    <small id="comment-error" class="help-blog"></small>
                                    <button class="btn btn-primary" id="btn-comment">submit</button>
                                </div>
                                <h3>các bình luận</h3>
                                <div id="comment">
                                    <div class="media-body">
                                        <?php $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <h4 class="media-heading"> <?php echo e(Auth::user()->name); ?></h4>
                                            <p><?php echo e($comment->content); ?></p>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('js'); ?>
<script>
    let _csrf = '<?php echo e(csrf_token()); ?>';
    $('#btn-comment').click(function (ev){
        ev.preventDefault();
        let content = $('#comment-content').val();
        let _commentUrl = '<?php echo e(route("ajax.comment",$course->id)); ?>';
        // console.log(content,_commentUrl);
        $.ajax({
            url: _commentUrl,
            type: 'POST',
            data:{
                content:content,
                _token: _csrf,
            },
            success:function (res) {
                if(res.error){
                   $('#comment-error').html(res.error)
                }else {
                    $('#comment-error').html('');
                    $('#comment-content').val('');
                    $('#comment').html(res);
                   // console.log(res);
                }
            }
        })
    })

</script>
<?php $__env->stopSection(); ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php echo $__env->make('admin.layouts_v2.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\unica\resources\views/admin/test_comment/test.blade.php ENDPATH**/ ?>