
<?php $__env->startSection('title', 'Home'); ?>
<?php $__env->startSection('content'); ?>
    <div class="col-md-11 main__right">
        <!-- Content here -->
        <div class="container-fluid">
            <div class="slider row">
                <div class="col-md-6 slider__content">
                    <div class="slider__title">
                        Khóa học HTML CSS
                    </div>
                    <div class="slider__desc">
                        Đây là khóa học đầy đủ và chi tiết nhất bạn có thể tìm thấy được ở trên Internet!
                    </div>

                    <a class="btn slider__btn">
                        Tìm hiểu thêm
                    </a>
                </div>
                <img src="../front/assets/img/slider.jpg" alt="" class="slider__img">
            </div>
        </div>

        <div class="box-learn container">
            <div class="row box-top">
                <div class="box__title">
                    Khóa Học nổi bật
                </div>
            </div>
            <div class="row box-bottom">
                <?php $__currentLoopData = $coursesPaginate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-3 box__item">
                        <div class="box__item-top">
                            <img src="../front/img/course_image/<?php echo e($course->image); ?>" alt="" class="box__item-img">
                            <div class="overlay-black"></div>
                            <a href="./student/<?php echo e($course->id); ?>" class="box__item-top--btn">
                                Xem khóa học
                            </a>
                        </div>
                        <h3 class="box__item-title">
                            <?php echo e($course->name); ?>

                        </h3>
                        <div class="box__item-price">
                                    <span class="box__item-price--old">
                                        2.499.000đ
                                    </span>
                            <span class="box__item-price--new">
                                        1.299.000đ
                                    </span>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

        </div>

        <div class="box-learn container">
            <div class="row box-top">
                <div class="box__title">
                    Tất cả khóa học
                </div>
            </div>
            <div class="row box-bottom">
                <?php $__currentLoopData = $courses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $course): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-3 box__item">
                        <div class="box__item-top">
                            <img src="../front/img/course_image/<?php echo e($course->image); ?>" alt="" class="box__item-img">
                            <div class="overlay-black"></div>
                            <a href="./student/<?php echo e($course->id); ?>" class="box__item-top--btn">
                                Xem khóa học
                            </a>
                        </div>
                        <h3 class="box__item-title">
                            <?php echo e($course->name); ?>

                        </h3>
                        <div class="box__item-price">
                                    <span class="box__item-price--old">
                                        2.499.000đ
                                    </span>
                            <span class="box__item-price--new">
                                        1.299.000đ
                                    </span>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

        </div>
        <!-- END -->
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\unica\resources\views/front/index.blade.php ENDPATH**/ ?>