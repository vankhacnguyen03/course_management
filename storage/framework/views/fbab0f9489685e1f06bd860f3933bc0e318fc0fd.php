
<?php $__env->startSection('body'); ?>
    <!-- Main -->
    <div class="app-main__inner">
        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">
                    <div class="card-body display_data">
                        <div class="position-relative row form-group">
                            <label for="" class="col-md-3 text-md-right col-form-label">Images</label>
                            <div class="col-md-9 col-xl-8">
                                <ul class="text-nowrap overflow-auto" id="images">
                                    <li class="d-inline-block mr-1" style="position: relative;">
                                        <img style="height: 150px;" src="./front/img/course_image/<?php echo e($course->image); ?>"
                                             alt="Image">
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label">Name</label>
                            <div class="col-md-9 col-xl-8">
                                <p><?php echo e($course->name); ?></p>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label">Description</label>
                            <div class="col-md-9 col-xl-8">
                                <p><?php echo e($course->description); ?></p>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label">Content</label>
                            <div class="col-md-9 col-xl-8">
                                <?php if($course->content != null): ?>
                                    <p><?php echo e($course->content); ?></p>
                                <?php else: ?>
                                    <p>Chưa có dữ liệu</p>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label">Category</label>
                            <div class="col-md-9 col-xl-8">
                                <p><?php echo e($course->category->name); ?></p>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label">Author</label>
                            <div class="col-md-9 col-xl-8">
                                <p><?php echo e($course->user->name); ?></p>
                            </div>
                        </div>

                        <div class="position-relative row form-group">
                            <label for="brand_id"
                                   class="col-md-3 text-md-right col-form-label">Status</label>
                            <div class="col-md-9 col-xl-8">
                                <?php if($course->status == 1): ?>
                                    <p>Show</p>
                                <?php else: ?>
                                    <p>Hide</p>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                            data-bs-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                        Lesson: <?php echo e(count($lessons)); ?>

                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show"
                                     aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <?php $__currentLoopData = $lessons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lesson): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="accordion-body">
                                            <?php echo e($lesson->name); ?>



                                            <form class="d-inline" action="./admin/lessons/delete/<?php echo e($lesson->id); ?>" method="post">
                                                <?php echo csrf_field(); ?>
                                                <?php echo method_field('delete'); ?>
                                                <button
                                                    class="btn btn-hover-shine btn-outline-danger border-0 btn-md float-right"
                                                    type="submit" data-toggle="tooltip" title="Delete"
                                                    data-placement="bottom"
                                                    onclick="return confirm('Do you really want to delete this item?')">
                                                            <span class="btn-icon-wrapper opacity-8">
                                                                <i class="fa fa-trash fa-w-20"></i>
                                                            </span>
                                                </button>

                                                <a href="./admin/lessons/edit/<?php echo e($lesson->id); ?>" data-toggle="tooltip"
                                                   title="Edit"
                                                   data-placement="bottom"
                                                   class="btn btn-outline-warning border-0 btn-md float-right">
                                                        <span class="btn-icon-wrapper opacity-8">
                                                            <i class="fa fa-edit fa-w-20"></i>
                                                        </span>
                                                </a>

                                                <a href="./admin/lessons/show/<?php echo e($lesson->id); ?>"
                                                   class="btn btn-hover-shine btn-primary border-0 btn-md float-right">
                                                    Details
                                                </a>
                                            </form>

                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\unica\resources\views/admin/course/show.blade.php ENDPATH**/ ?>