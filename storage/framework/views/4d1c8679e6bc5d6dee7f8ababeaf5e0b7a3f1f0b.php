
<?php $__env->startSection('body'); ?>
    <!-- Main -->
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-ticket icon-gradient bg-mean-fruit"></i>
                    </div>
                    <div>
                        Questions
                        <div class="page-title-subheading">
                            View, create, update, delete and manage.
                        </div>
                    </div>
                </div>

                <div class="page-title-actions">
                    <a href="./admin/questions/create" class="btn-shadow btn-hover-shine mr-3 btn btn-primary">
                                    <span class="btn-icon-wrapper pr-2 opacity-7">
                                        <i class="fa fa-plus fa-w-20"></i>
                                    </span>
                        Create
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="main-card mb-3 card">

                    <div class="card-header">

                        <form>
                            <div class="input-group">
                                <input type="search" name="search" id="search"
                                       placeholder="Search everything" class="form-control">
                                <span class="input-group-append">
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="fa fa-search"></i>&nbsp;
                                                    Search
                                                </button>
                                            </span>
                            </div>
                        </form>

                        <div class="btn-actions-pane-right">
                            <div role="group" class="btn-group-sm btn-group">
                                <button class="btn btn-focus">This week</button>
                                <button class="active btn btn-focus">Anytime</button>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="align-middle mb-0 table table-borderless table-striped table-hover">
                            <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">question</th>
                                <th class="text-center">test_name</th>


                                <th class="text-center">Actions</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $question): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr class="even pointer">
                                    <td class="text-center text-muted"><?php echo e($question->id); ?></td>
                                    <td class="text-center"><?php echo e($question->question); ?></td>
                                    <td class="text-center"><?php echo e($question->test->title); ?></td>


                                    <td class="text-center">


                                        <a href="/admin/questions/edit/<?php echo e($question->id); ?>" data-toggle="tooltip"
                                           title="Edit"
                                           data-placement="bottom"
                                           class="btn btn-outline-warning border-0 btn-sm">
                                                        <span class="btn-icon-wrapper opacity-8">
                                                            <i class="fa fa-edit fa-w-20"></i>
                                                        </span>
                                        </a>

                                        <a href="/admin/questions/delete/<?php echo e($question->id); ?>"
                                           class="btn btn-hover-shine btn-outline-danger border-0 btn-sm"
                                           type="submit" data-toggle="tooltip" title="Delete"
                                           data-placement="bottom"
                                           onclick="return confirm('Do you really want to delete this item?')">
                                                            <span class="btn-icon-wrapper opacity-8">
                                                                <i class="fa fa-trash fa-w-20"></i>
                                                            </span>
                                        </a>


                                        <a href="/admin/questions/show/<?php echo e($question->id); ?>"
                                           class="btn btn-hover-shine btn-outline-primary border-0 btn-sm">
                                            <span class="btn-icon-wrapper opacity-8">  <i
                                                    class="fa fa-info-circle fa-w-20"></i></span>
                                        </a>

                                        <a href="/admin/questions/show/<?php echo e($question->id); ?>"
                                           class="btn btn-hover-shine btn-outline-primary border-0 btn-sm">
                                            <span class="btn-icon-wrapper opacity-8">  <i
                                                    class="fa fa-heart fa-w-20"></i></span>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!-- End Main -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\unica\resources\views/admin/question/index.blade.php ENDPATH**/ ?>