
<?php $__env->startSection('title', 'Show'); ?>
<?php $__env->startSection('content'); ?>
    <div class="col-md-11 main__right">
        <!-- Content here -->
        <div class="container detail">
            <div class="row">
                <div class="col-md-8">
                    <div class="detail__head">
                        <div class="detail__head-name">
                            <?php echo e($course->name); ?>

                        </div>

                        <div class="detail__head-desc">
                            <?php echo e($course->description); ?>

                        </div>
                    </div>

                    <div class="detail__body">
                        <div class="detail__body-title">
                            Nội dung khóa học
                        </div>

                        <ul class="detail__body-list">
                            <?php if($studentCourse != null): ?>
                                <?php $__currentLoopData = $lessons; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $lesson): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li class="detail__body-item">
                                        <a href="./<?php echo e($lesson->id); ?>/learn" class="detail__body-link">
                                            <?php echo e($key + 1); ?>. <?php echo e($lesson->name); ?>

                                        </a>
                                    </li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php else: ?>
                                <li class="detail__body-item">
                                    <a href="" class="detail__body-link">
                                        Bạn chưa đăng ký khóa học
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>

                    <div class="detail__relate">
                        <div class="detail__relate-title">
                            Khóa học liên quan
                        </div>

                        <a href="/admin/student" class="detail__relate-item">
                            <img src="../../front/assets/img/relate.png" alt="" class="detail__relate-img">
                            <div class="detail__relate-right">
                                <div class="detail__relate-name">
                                    HTML CSS Pro
                                </div>
                                <div class="detail__relate-price">
                                    <p class="detail__relate-price--old">2.499.000đ</p>
                                    <p class="detail__relate-price--new">1.299.000đ</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="detail__right">
                        <img src="../../front/img/course_image/<?php echo e($course->image); ?>" alt="" class="detail__right-img">
                            <?php if($studentCourse != null): ?>
                                <a href="./<?php echo e($first_lesson->id); ?>/learn" class="detail__right-btn">
                                    Vào học
                                </a>
                            <?php else: ?>
                                <a href="" class="detail__right-btn">
                                    Đăng ký học
                                </a>
                            <?php endif; ?>

                        <div class="detail__right-content">
                            <div class="detail__right-item">
                                <div class="detail__right-icon">
                                    <i class="fa-sharp fa-solid fa-graduation-cap"></i>
                                </div>
                                <div class="detail__right-desc">Trình độ cơ bản</div>
                            </div>

                            <div class="detail__right-item">
                                <div class="detail__right-icon">
                                    <i class="fa-sharp fa-solid fa-film"></i>
                                </div>
                                <div class="detail__right-desc"><?php echo e(count($lessons)); ?> bài học</div>
                            </div>

                            <div class="detail__right-item">
                                <div class="detail__right-icon">
                                    <i class="fa-solid fa-battery-three-quarters detail__right-icon"></i>
                                </div>
                                <div class="detail__right-desc">Trình độ cơ bản</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END -->
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('front.layout.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\xampp\htdocs\unica\resources\views/front/show.blade.php ENDPATH**/ ?>