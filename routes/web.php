<?php

use App\Http\Controllers\Admin\AjaxCommentController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CourseController;
use App\Http\Controllers\Admin\LearnController;
use App\Http\Controllers\Admin\LessonController;
use App\Http\Controllers\Admin\QuestionController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\StudentExamController;
use App\Http\Controllers\Admin\TestController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\VideoController;
use App\Http\Controllers\FileUploadController;
use App\Http\Controllers\Front\HomeController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*



|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);

Route::get('/layout_v2', function () {
    return view('admin.layouts_v2.master');
});

Auth::routes();

Route::prefix('student')->group(function () {
    Route::get('/', [HomeController::class, 'index']);
    Route::get('/{id}', [HomeController::class, 'show']);
    Route::get('/{id}/learn', [HomeController::class, 'learn']);
});

Route::middleware('auth')->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('/', [UserController::class, 'index'])->name('admin');

        Route::prefix('course')->group(function () {
            Route::get('/', [CourseController::class, 'index']);
            Route::get('/create', [CourseController::class, 'create']);
            Route::post('/', [CourseController::class, 'store']);
            Route::get('/{id}', [CourseController::class, 'show']);
            Route::get('/{id}/edit', [CourseController::class, 'edit']);
            Route::put('/{id}/edit', [CourseController::class, 'update']);
            Route::delete('/{id}', [CourseController::class, 'destroy']);
            Route::get('/{id}/create', [LessonController::class, 'create']);
            Route::post('/{id}/create', [LessonController::class, 'store']);
        });

        Route::prefix('studentexam')->group(function () {
            Route::get('/',[StudentExamController::class,'index'])->name('student.exam');
            Route::get('/{id}', [StudentExamController::class, 'show']);
            Route::post('/{id}', [StudentExamController::class, 'store']);
            Route::get('/resultdetail/{id}',[StudentExamController::class,'showResultAnswer']);
        });

        Route::resource('users', UserController::class);

        Route::prefix('test')->group(function () {
            Route::get('/', [TestController::class, 'index']);
            Route::get('/create', [TestController::class, 'create']);
            Route::post('/', [TestController::class, 'store']);
            Route::get('/{id}', [TestController::class, 'show']);
            Route::get('/{id}/edit', [TestController::class, 'edit']);
            Route::put('/{id}', [TestController::class, 'update']);
            Route::delete('/{id}', [TestController::class, 'destroy']);
        });

        Route::resource('roles', RoleController::class);

        Route::prefix('learn')->group(function () {
            Route::get('/', [LearnController::class, 'index']);
            Route::get('/{id}/create', [LearnController::class, 'create']);
            Route::post('/{id}/create', [LearnController::class, 'store']);
            Route::get('/{id}', [LearnController::class, 'show']);
        });


        Route::prefix('categories')->group(function () {
            Route::get('/', [CategoryController::class, 'index'])->name('list_categories');
            Route::get('add', [CategoryController::class, 'create']);
            Route::post('add', [CategoryController::class, 'store']);
            Route::get('/show/{id}', [CategoryController::class, 'show']);
            Route::get('/edit/{id}', [CategoryController::class, 'edit']);
            Route::put('/edit/{id}', [CategoryController::class, 'update']);
            Route::delete('delete/{id}', [CategoryController::class, 'destroy']);
        });


        Route::prefix('lessons')->group(function () {
            Route::get('/', [LessonController::class, 'index'])->name('admin.lesson.index');
            Route::get('create', [LessonController::class, 'create']);
            Route::post('create', [LessonController::class, 'store'])->name('admin.lesson.create');
            Route::get('show/{id}', [LessonController::class, 'show']);
            Route::get('/edit/{id}', [LessonController::class, 'edit']);
            Route::put('/edit/{id}', [LessonController::class, 'update']);
            Route::get('delete/{id}', [LessonController::class, 'destroy']);
        });

        Route::prefix('questions')->group(function () {
            Route::get('/', [QuestionController::class, 'index'])->name('admin.question.index');
            Route::get('create', [QuestionController::class, 'create']);
            Route::post('create', [QuestionController::class, 'store'])->name('admin.question.create');
            Route::get('show/{id}', [QuestionController::class, 'show']);
            Route::get('/edit/{id}', [QuestionController::class, 'edit']);
            Route::put('/edit/{id}', [QuestionController::class, 'update']);
            Route::get('delete/{id}', [QuestionController::class, 'destroy']);
        });



        //video
        Route::prefix('videos')->group(function () {
            Route::get('/', [VideoController::class, 'index'])->name('list_videos');
            Route::get('/add-video', [VideoController::class, 'create']);
            Route::post('/add-video', [VideoController::class, 'store']);
            Route::delete('/delete/{id}', [VideoController::class, 'destroy']);
        });
        //upload

        Route::post('file-upload/upload-large-files', [FileUploadController::class, 'uploadLargeFiles'])->name('files.upload.large');

        Route::prefix('ajax')->group(function (){
            Route::get('',[AjaxCommentController::class,'index'])->name('ajax.index');
            Route::post('/comment/{course_id}',[AjaxCommentController::class,'comment'])->name('ajax.comment');
        });
    });
});




